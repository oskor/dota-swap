(function() {
  'use strict';

  angular
    .module('swap')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
