(function() {
  'use strict';

  angular
    .module('swap')
    .controller('FantasyController', FantasyController);

  /** @ngInject */
  function FantasyController($scope, $filter, dotaService, $interval, $firebaseObject) {
    var vm = this;
    var _players = [];
    vm.league_id = "4664";
    vm.teams = [];
    vm.players = [];

    vm.sortOptions = [{name:'Name',id:'name'}, {name:'Team',id:'team_name'}, {name: "Kills", id: 'stats.fantasy.kills'}
    , {name:'Deaths',id:'stats.fantasy.deaths'}, {name:'Creep Score',id:'stats.fantasy.last_hits'}, {name:'GPM',id:'stats.fantasy.gpm'}
    , {name:'Fight%',id:'stats.fantasy.team_fight'}, {name:'Runes',id:'stats.fantasy.runes'}, {name:'FB',id:'stats.fantasy.first_blood'}, {name:'Wards',id:'stats.fantasy.wards'}
    , {name:'Towers',id:'stats.fantasy.towers'}, {name:'Total',id:'stats.fantasy.total'}];
    vm.sortBy = 11;
    vm.predicate = vm.sortOptions[vm.sortBy].id;
    vm.reverse = true;
    vm.order = function(index) {
      vm.reverse = (vm.sortBy === index) ? !vm.reverse : false;
      vm.sortBy = index;
      vm.predicate = vm.sortOptions[vm.sortBy].id;
    };
    vm.filter = {};
    vm.filterOptions = [{name:'Role',filters:[{name: 'Core', data: {'fantasy_role': 1}},{name: 'Support', data: {'fantasy_role': 2}}]}];
    vm.filterPlayers = function(filter) {
      vm.filter = filter;
      vm.sets = _.filter(_players, filter);
    };

    firebase.database().ref('fantasy').on('value', function(teams) {
      $scope.$apply(function() {
        vm.teams = teams.val().teams;
        _players = _.flatten(_.map(vm.teams, function(v, i) {return _.map(v.members, function(v,i){return v;});}));
        vm.players = _players;
        console.log(_players);
      });
    });
    
    

  }
})();
