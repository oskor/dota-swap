'use strict';

angular.module('swap').factory('itemRepo', ['simpleResource', function(simpleResource) {
  
  function getAccountItems(steamid) {
    return simpleResource.newResource().withKey().withParam('steamid',steamid).get('/IEconItems_570/GetPlayerItems/v1/').then(function(items) {
        return items.result.items;
      });
  }

  return {
    getItems: function() {
      return simpleResource.newResource().get('app/data/items.json');
    },
    getSets: function() {
      return simpleResource.newResource().get('app/data/sets.json');
    },
    getAccountItems: getAccountItems,
    getMyItems: function() {
      //return simpleResource.get('app/data/myitems.json');
      return getAccountItems("76561197964671190");
    },
    getKachonItems: function() {
      return getAccountItems("76561197990490323");
    }
  };
}]);