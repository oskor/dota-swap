(function() {
  'use strict';

  angular
    .module('swap')
    .controller('FriendsController', FriendsController);

  /** @ngInject */
  function FriendsController($filter,friendsService, dotaService) {
    var vm = this;
    vm.sortOptions = [{name:'Name',id:'personaname'}, {name:'Real Name',id:'realname'}, {name: "DOTA", id: 'dota'}, {name:'Last Logoff',id:'lastlogoff'}, {name:'Friend Since',id:'friendship.friend_since'}, {name:'Created',id:'timecreated'}];
    vm.sortBy = 0;
    vm.predicate = vm.sortOptions[vm.sortBy].id;

    vm.reverse = true;
    vm.order = function(index) {
      vm.reverse = (vm.sortBy === index) ? !vm.reverse : false;
      vm.sortBy = index;
      vm.predicate = vm.sortOptions[vm.sortBy].id;
    };

    vm.steamid = "76561197964671190";
    vm.friends = [];
    friendsService.getFriends(vm.steamid).then(function(friends) {
      friendsService.getDotaFriends(vm.steamid).then(function(friendsWDota) {
        vm.friends = friendsWDota; // $filter('orderBy')(friends, vm.predicate());
      })
    });

    //vm.dotaFriends = [];
    //dotaService.getDotaFriends('76561197964671190').then(function(dotaFriends) {
    //  vm.dotaFriends = dotaFriends;
    //});


    
    
  }
})();
