'use strict';

angular.module('swap').factory('friendsService', ['_', '$q', 'friendsRepo', 'dotaRepo', function(_, $q, friendsRepo, dotaRepo) {
    var self = {};
    var _steamId;
    var _friends;
    var _friendship;

    
    function makeBig(num) { return { num: num, numL: num.substr(0, 15), numR: num.substr(15, 2) }; }

    function makeAccountId(num1) {
        var num2 = '76561197960265728';
        if(num1 && num2) {
            var big1 = makeBig(num1.toString());
            var big2 = makeBig(num2.toString());
            if(big1.numL && big2.numL) {
                var first = big1.numL - big2.numL;
                var second = big1.numR - big2.numR;
                first = first + '00';
                return parseInt(first) + parseInt(second);
            }
        }
        return 0;
    }

    function loadFriends(steamid) {
        _steamId = steamid;
        return friendsRepo.getFriends(steamid).then(function(friends) {
            _friendship = _.map(friends, function(friend) {
                friend.date = new Date(friend.friend_since * 1000);
                return friend;
            });
            
            var playersPromises = _.chunk(_.pluck(friends, 'steamid'),99).map(function(friendChunk) {
                return getPlayerSummaries(friendChunk).then(function(players) {
                    return players;
                });
            });
            return $q.all(playersPromises).then(function(allFriendProfiles) {
                return _(allFriendProfiles).flatten().union().value();
            })
        });
    }

    function getPlayerSummaries(steamIds) {
        return friendsRepo.getPlayerSummaries(steamIds.join()).then(function(players) {
            return _.map(players, function(friend) {
                return _.extend(friend, {friendship:_.find(_friendship, {steamid: friend.steamid}) }, {account_id: makeAccountId(friend.steamid)});
            });
        });
    }

    function markDotaFriends(steamid) {
        return dotaRepo.getMatchHistory(makeAccountId(steamid)).then(function(matches) {
            var players = _(matches).pluck('players').flatten().pluck('account_id').value();
            var intersection = _.intersection(players, _(_friends).pluck('account_id').value());
            return _friends = _.map(_friends, function(friend) {
                return _.extend(friend, {dota: _.includes(intersection, friend.account_id)});
            });
        });
    }

    function getDotaFriends(steamid) {
        var steamid = steamid;
        //search last 100 games, return all friends that played with account.
        return getFriends(steamid).then(function(friends) {
            return markDotaFriends(steamid).then(function (dotaFriends) {
                return _.filter(dotaFriends, {'dota': true});
            });
        });
            
    }


    function getFriends(steamid) {
        if(_friends==undefined || steamid != _steamId)
        {
            return loadFriends(steamid).then(function(friends) {
                return _friends = _.map(friends, function(friend) {
                    return _.extend(friend, {friendship:_.find(_friendship, {steamid: friend.steamid}) });
                });
            });
        }
        else
        {
            return $q.when(_friends);
        }
    }

    function loadFriendItems() {
        
    }

    return {
        getFriends: getFriends,
        makeAccountId: makeAccountId,
        markDotaFriends: markDotaFriends,
        getDotaFriends: getDotaFriends
    };
}
]);

//friend_since: 1260251205
//relationship: "friend"
//steamid: "76561197960409438"