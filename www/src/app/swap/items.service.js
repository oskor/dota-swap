'use strict';

angular.module('swap').factory('itemService', ['_', '$q', 'itemRepo', function(_, $q, itemRepo) {
    function Owned(itms){ return _.every(itms, {iOwn: true}); }
    function Some(itms) {
        return _.some(itms, {
            iOwn: true
        });
    }

    function markMyItems() {
        return _.map(sets, function(set) {
            return _.extend(set, {
                color: "mythical",
                owned: Owned(set.items),
                some: Some(set.items),
                items: _.map(set.items, function(item) {
                    return _.extend(item, {
                        iOwn: _.some(myItems, {
                            defindex: item.id
                        })
                    })
                })
            });
        });
    }

    function cleanName(item) {
        if(item && item.hero) {
            item.image = "assets/images/" + item.image_inventory + ".png";
            item.hero = item.hero.replace("npc_dota_hero_", "");
            item.hero_image = 'http://cdn.dota2.com/apps/dota2/images/heroes/' + item.hero.toLowerCase() + '_sb.png';
            item.hero_image_mini = 'assets/images/miniheroes/' + item.hero.toLowerCase() + '.png';
            item.hero = _.startCase(item.hero);
        }
        
        return item;
    }
    
    function cleanSet(set) {
        set = cleanName(set);

        _.map(set.items, cleanName);
        return set;
    }

    function cleanSets(sets) {
        return _.map(sets, cleanSet);
    }

    return {
        cleanSets: cleanSets
    };
}]);