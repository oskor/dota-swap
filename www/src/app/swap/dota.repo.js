'use strict';

angular.module('swap').factory('dotaRepo', ['simpleResource', function(simpleResource) {
  var friends = [];


  return {
    getMatchHistory: function(account_id) {
      return simpleResource.newResource().withKey().withParam('account_id',account_id).get('/IDOTA2Match_570/GetMatchHistory/V001/').then(function(matches) {
        return matches.result.matches;
      });
    }
  };
}]);

//4405456 --- it thinks based off steamid

//4405462 --- mine