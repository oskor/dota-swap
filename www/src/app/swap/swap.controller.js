(function() {
  'use strict';

  angular
    .module('swap')
    .controller('SwapController', SwapController);

  /** @ngInject */
  function SwapController(swapService, friendsService, toastr) {
    var vm = this;
    var opt = {
      sort: "name",
      filter: "all"
    };
    vm.mySteamId = "76561197964671190";
    //Oskor 76561197964671190
    //Nyte  76561197984833457

    vm.setup = function() {
      vm.selectedFriend = null;
      vm.sets = [];
      vm.friends = [];
      swapService.setup(vm.mySteamId).then(function(data) {
        vm.sets = swapService.getSets(opt);
      });

      friendsService.getDotaFriends(vm.mySteamId).then(decorateFriends).then(function(friends) {
        vm.friends = friends;
      });
    }

    vm.setup();

    vm.compareItemsWithFriend = function(friend) {
      friend.items.loaded = false;
      friend.items.sets = [];
      friend.items.loading = true;
      friend.items.error = false;
      vm.selectedFriend = friend.steamid;
      swapService.compareItemsToFriend(friend.steamid).then(function(loaded) {
        vm.sets = swapService.getSets(opt);
        friend.items.loading = false;
        if(!loaded) {
          friend.items.error = true;
          //toastr.error('Failed to load friend DOTA items. Ask friend to make Steam Items Public.');
        }
        if(loaded) {
          friend.items.loaded = loaded;
          friend.items.sets = swapService.getSets({filter: "fcomplete", sort: opt.sort});
          //toastr.success("Loaded Friend's DOTA Items.");
        }
      });
    }

    vm.allSets = function() {
      vm.sets = swapService.getAllSets();
    };
    vm.filterSets = function(filter) {
      opt.filter = filter;
      vm.sets = swapService.getSets(opt);
    };
    vm.sortBy = function(sort) {
      opt.sort = sort;
      vm.sets = swapService.getSets(opt);
    };

    function decorateFriends(friends) {
      return _(friends).map(function(friend) {
        return _.extend(friend, {
          items: {
            loading: false,
            loaded: false,
            error: false,
            sets: []
          }
        });
      }).value();
    }

  }
})();