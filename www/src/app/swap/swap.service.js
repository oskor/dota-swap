'use strict';

angular.module('swap').factory('swapService', ['_', '$q', 'itemRepo', 'itemService', 'friendsService', function(_, $q, itemRepo, itemService, friendsService) {
  var sets = [];
  var items = [];
  var mySets = [];
  
  var opt = {
    sort: "name",
    filter: undefined
  };

  function Own(set) {
    return _.every(set.items, {
      iOwn: true
    });
  }
  function fOwn(set) {
    return _.every(set.items, {
      fOwn: true
    });
  }

  function Some(set) {
    return _.some(set.items, {
      iOwn: true
    });
  }
  function fSome(set) {
    return _.some(set.items, {
      fOwn: true
    });
  }

  function Completes(set) {
    var completeSet = _.every(set.items, function(item) {
      return (item.iOwn || item.fOwn);
    });
    if(completeSet && !set.Ownership.iOwn){
      console.log(set);
    }
    return (completeSet && !set.Ownership.iOwn && set.Ownership.iSome);
  }

  function Pct() {
    return _.some(this.items, {
      iOwn: true
    });
  }

  //Sorting Functions
  function sortSets(sort) {
    if (sort == "have")
      return [numHave];
    if (sort == "miss")
      return [numMiss];
    else
      return [sort];
  }
  function sortOrder(sort) {
    if (sort == "have")
      return ["desc"];
    if (sort == "miss")
      return ["desc"];
    else
      return ["asc"];
  }

  function name(set) {
    return "name";
  }

  function numHave(set) {
    return _(set.items).pluck("iOwn").map(function(iown) {
      return iown ? 1 : 0
    }).reduce(function(total, num) {
      return total + num;
    });
  }

  function numMiss(set) {
    return _(set.items).pluck("iOwn").map(function(iown) {
      return iown ? 0 : 1
    }).reduce(function(total, num) {
      return total + num;
    });
  }

  function markMyItems(myItems) {
    return _.map(sets, function(set) {
      _.extend(set, {
        items: _.map(set.items, function(item) {
          return _.extend(item, {
            iOwn: _.some(myItems, {
              defindex: item.id
            })
          })
        })
      });
      return _.extend(set, {
        color: "mythical",
        Ownership: {
          iOwn: Own(set),
          iSome: Some(set)
        }
      });
    });
  }

  function markFriendItems(fItems) {
    return _.map(sets, function(set) {
      _.extend(set, {
        items: _.map(set.items, function(item) {
          return _.extend(item, {
            fOwn: _.some(fItems, {
              defindex: item.id
            })
          })
        })
      });
      _.extend(set, {
        color: "mythical",
        Ownership: _.extend(set.Ownership, {
          fOwn: fOwn(set),
          fSome: fSome(set)
        })
      });
      return _.extend(set, {
        Ownership: _.extend(set.Ownership, {
          fCompletes: Completes(set)
        })
      })
    });
  }

  //Filter Functions
  function filterSets(filter) {
    if (filter == "complete") {
      return (function(set) {
        return set.Ownership.iOwn;
      });
    }
    if (filter == "some") {
      return (function(set) {
        return set.Ownership.iSome && !set.Ownership.iOwn;
      });
    }
    if (filter == "fcomplete") {
      return (function(set) {
        return set.Ownership.fCompletes;
      });
    }
    return (function(set) {
      return true
    });
  }

  function getSets(options) {
    var filter = filterSets(options.filter);
    var sort = sortSets(options.sort);
    var order = sortOrder(options.sort);
    return _(mySets).filter(filter).sortByOrder(sort,order).value();
  }

  function setup(steamid) {
    return $q.all([itemRepo.getSets(), itemRepo.getItems(), itemRepo.getAccountItems(steamid)]).then(function(data) {
      sets = itemService.cleanSets(_.cloneDeep(data[0]));
      items = data[1];
      var myItems = data[2];

      mySets = markMyItems(myItems);
      return data;
    });
  }

  function compareItemsToFriend(steamid) {
    return itemRepo.getAccountItems(steamid).then(function(fItems) {
      mySets = markFriendItems(fItems);
      if(fItems){return true;}
      return false;
    });
  }

  var iOwn = function(set) {
    _.every(set.items, {
      iOwn: true
    });
  };



  return {
    getSet: function(id) {
      return _.find(sets, {
        'id': id
      });
    },
    getItem: function(id) {
      return _.find(items, {
        'id': id
      });
    },
    getAllSets: function() {
      return sets;
    },
    getAllItems: function() {
      return items;
    },
    getAllMyItems: function() {
      return myItems;
    },
    getMySets: function() {
      return _.filter(mySets, function(set) {
        return set.some();
      });
    },
    getMyIncompleteSets: function() {
      return _(mySets).filter(function(set) {
        return set.some() && !set.own();
      }).value();
    },
    getMyCompletedSets: function() {
      return _.filter(mySets, function(set) {
        return set.own();
      });
    },
    getSets: getSets,
    sortSets: sortSets,
    iOwn: iOwn,
    compareItemsToFriend: compareItemsToFriend,
    setup: setup
  };
}]);