'use strict';

angular.module('swap').factory('friendsRepo', ['simpleResource', function(simpleResource) {
  var friends = [];


  return {
    getFriends: function(steamid) {
      return simpleResource.newResource().withKey().withParam('steamid',steamid).get('/ISteamUser/GetFriendList/v1/').then(function(friends) {
        return friends.friendslist.friends;
      });
      //return simpleResource.get('app/data/friends.json').then(function(friends) {
      //      return friends["friends"];
      //  });
    },
    getPlayerSummaries: function(ids) {
      return simpleResource.newResource().withKey().withParam('steamids',ids).get('/ISteamUser/GetPlayerSummaries/v2/').then(function(players) {
        return players.response.players;
      });
        //return simpleResource.newResource().get('app/data/friends.json').then(function(friends) {
        //    return friends["players"];
        //});
    }
  };
}]);