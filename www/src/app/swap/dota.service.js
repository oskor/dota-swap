'use strict';

angular.module('swap').factory('dotaService', ['_', '$q', 'dotaRepo', 'friendsService', function(_, $q, dotaRepo, friendsService) {
    var self = {
        steamId: 0,
        dotaFriends: []
    };

    function getAccountIds(friends) {
        return $q.when(_.map(friends, steamIdToAccountId));
    }

    function intersectFriendsWithTeam() {
        return _.intersect()
    }

    function steamIdToAccountId(account) {
        return friendsService.makeAccountId(account.steamid);
    }

    self.getDotaFriends = function(steamid) {
        var friends = [];
        self.steamId = steamid;
        //search last 100 games, return all friends that played with account.
        return friendsService.getFriends(self.steamId).then(function(friends) {
            self.accountIds = _(friends).pluck('account_id').value();
            friends = friends;
            return dotaRepo.getMatchHistory(steamIdToAccountId({steamid: self.steamId})).then(function(matches) {
                var players = _(matches).pluck('players').flatten().pluck('account_id').value();
                var intersection = _.intersection(players,self.accountIds);
                var dotaBuddies = _.filter(friends, function(account) {
                    return _.includes(intersection, account.account_id);
                });
                return dotaBuddies;
            });
        });
    };

    return self;
}
]);

//76561197964671190
//76561197960265728

/*
"match_id": 1853367193,
    "match_seq_num": 1641170250,
    "start_time": 1444370610,
    "lobby_type": 0,
    "radiant_team_id": 0,
    "dire_team_id": 0,
    "players": [
        {
            "account_id": 4294967295,
            "player_slot": 0,
            "hero_id": 35
        },
        {
            "account_id": 4294967295,
            "player_slot": 1,
            "hero_id": 25
        },
        {
            "account_id": 121059740,
            "player_slot": 2,
            "hero_id": 3
        },
        {
            "account_id": 86717333,
            "player_slot": 3,
            "hero_id": 21
        },
        {
            "account_id": 4294967295,
            "player_slot": 4,
            "hero_id": 37
        },
        {
            "account_id": 124094583,
            "player_slot": 128,
            "hero_id": 110
        },
        {
            "account_id": 4584530,
            "player_slot": 129,
            "hero_id": 68
        },
        {
            "account_id": 4405462,
            "player_slot": 130,
            "hero_id": 45
        },
        {
            "account_id": 30224595,
            "player_slot": 131,
            "hero_id": 14
        },
        {
            "account_id": 21451156,
            "player_slot": 132,
            "hero_id": 67
        }
    ]
    
},
*/