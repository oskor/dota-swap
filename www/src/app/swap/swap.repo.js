'use strict';

angular.module('swap').factory('swapRepo', ['simpleResource', function(simpleResource) {
  return {
    getItems: function() {
      return simpleResource.get('app/data/items.json');
    },
    getSets: function() {
      return simpleResource.get('app/data/sets.json');
    },
    getMyItems: function() {
      return simpleResource.get('app/data/myitems.json');
    },
    getKachonItems: function() {
      return simpleResource.get('app/data/kachonitems.json');
    }
  };
}]);