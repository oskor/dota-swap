(function() {
  'use strict';

  angular
    .module('swap')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      }).state('swap', {
        url: '/swap',
        templateUrl: 'app/swap/swap.html',
        controller: 'SwapController',
        controllerAs: 'swap'
      }).state('friends', {
        url: '/friends',
        templateUrl: 'app/swap/friends.html',
        controller: 'FriendsController',
        controllerAs: 'fr'
      })
      .state('fantasy', {
        url: '/fantasy',
        templateUrl: 'app/fantasy/fantasy.html',
        controller: 'FantasyController',
        controllerAs: 'ft'
      });

    $urlRouterProvider.otherwise('/fantasy');
  }

})();
