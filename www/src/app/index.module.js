(function() {
  'use strict';

  angular
    .module('swap', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ui.router', 'firebase']);

})();
