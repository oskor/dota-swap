var kv = require('keyvalues-node');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');

var filePath = path.join(__dirname, 'items_game.txt');
var saveItemsPath = path.join(__dirname, 'items.json');
var saveSetsPath = path.join(__dirname, 'sets.json');

function iReduce(item, index) {
    if(Array.isArray(item.used_by_heroes)){ console.log(item.used_by_heroes);}
    var image = _.toLower(_.last(_.split(item.image_inventory, '/')));
    if(!image) {
        console.log('====================');
        console.log(item);
        image = "";
    }
    return _.extend({}, {
        id: parseInt(index),
        name: item.name,
        slot: item.item_slot,
        type: item.item_type_name,
        hero: _.keys(item.used_by_heroes)[0],
        rarity: item.item_rarity,
        bundle: (_.some(item.bundle)),
        item_name: item.item_name,
        image_inventory: item.image_inventory,
        image: image
    });
}

function sReduce(set, index) {
    return _.extend({}, {
        id: 0,
        name: set.store_bundle,
        items: _.map(set.items, function(item,index){ return index; }),
    });
}

function findItem(name, items) {
    var item = _.find(items, {'name': name});
    if(item) {
        return item;
    }
    return {id: -1, name: 'Not Found!'};
}

//adds setId to each item in a set.
function cleanItems(items, sets) {
    return _.flatten(_.map(sets, 'items'));
}

//adds setId to each item in a set.
function cleanSets(items, sets) {
    return _.map(sets, function(set, index) {
        nSet = findItem(set.name, items);
        _.extend(set, nSet);
        if(set.id>=0) {
            set.items = _.map(set.items, function(item, index) {
                var nItem = _.extend(findItem(item, items), {setid: set.id});
                return nItem;
            });
        }
        return set;
    });
}


function saveFile(file, data, text) {
    fs.writeFile(file, JSON.stringify(data), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log(text + " file was saved!");
    });
}

fs.readFile(filePath, {encoding: 'utf-8'}, function (error, data) {
    if(!error && data) {
        var oData = kv.decode(data);

        var items = _.map(oData.items_game.items, iReduce);
        var sets = _.map(oData.items_game.item_sets, sReduce);
        
        sets = cleanSets(items, sets);
        items = cleanItems(items, sets);
        saveFile(saveItemsPath, items, "Items");
        saveFile(saveSetsPath, sets, "Sets");

    }else{
        console.log("error: ");
        console.log(error);
    }
});