(function() {
   var pwd = "2468013579";
   var chars = "01234567";
   var guesses = [];
   var all = chars.split("");
   
   var start = Date.now() / 1000 | 0;
   console.log("START:", start);
   
   var nextGuess = dive([]);
   
   var end = Date.now() / 1000 | 0;
   
   console.log("END:", end);
   console.log("Got " + guesses.length + " guesses in " + (end - start) + " seconds");
   console.log("guesses: ", guesses[guesses.length-1]);
   
   function dive(stack){
      if(stack.length==all.length){
         guesses.push(stack.join(''));
         return;
      }
      all.forEach(function(v) {
         dive(stack.concat(v));
      });
      //for(var i=0;i<all.length;i++)
      //{
      //   dive(stack.concat(all[i]));
      //}
   }
}());