# README #

This simple Angular 1 app allows you to compare your DOTA 2 cosmetic items with your friends and shows what sets they can help you complete. You must set your Steam Inventory to the public before app will work for you.

### Angular 1 Web App ###

* Search and filter your DOTA 2 cosmetic items.
* Search your friends to find items that help you complete your sets.
* Found in the www folder.

### NodeJs Item Import ###

* A NodeJs app that imports Valves odd Key/Value text format and exports it as usable JSON objects.
* Found in the Import folder.

### How do I get set up? ###

* Install Node JS
* Open PowerShell
* Install gulp & bower `npm install -g gulp bower`
* You may have to also install gulp-cli `npm install -g gulp-cli`
* Run `npm install`
* Run `bower install`
* To start local server Run `gulp serve`