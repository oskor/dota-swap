'use strict';

var _ = require('lodash');
var q = require('q');
var fantasyRepo = require('./simple.resource')


var leagueId = 0;
var apiThrottle = 1000;
var _teams = [];
var _matches = [];
var _heroes = [{name:"neutral creep",id:999},{name:"anti-mage",id:1},{name:"axe",id:2},{name:"bane",id:3},{name:"bloodseeker",id:4},{name:"crystal maiden",id:5},{name:"drow ranger",id:6},{name:"earthshaker",id:7},{name:"juggernaut",id:8},{name:"mirana",id:9},{name:"shadow fiend",id:11},{name:"morphling",id:10},{name:"phantom lancer",id:12},{name:"puck",id:13},{name:"pudge",id:14},{name:"razor",id:15},{name:"sand king",id:16},{name:"storm spirit",id:17},{name:"sven",id:18},{name:"tiny",id:19},{name:"vengeful spirit",id:20},{name:"windranger",id:21},{name:"zuus",id:22},{name:"kunkka",id:23},{name:"lina",id:25},{name:"lich",id:31},{name:"lion",id:26},{name:"shadow shaman",id:27},{name:"slardar",id:28},{name:"tidehunter",id:29},{name:"witch doctor",id:30},{name:"riki",id:32},{name:"enigma",id:33},{name:"tinker",id:34},{name:"sniper",id:35},{name:"necrophos",id:36},{name:"warlock",id:37},{name:"beastmaster",id:38},{name:"queen of pain",id:39},{name:"venomancer",id:40},{name:"faceless void",id:41},{name:"void",id:41},{name:"wraith king",id:42},{name:"death prophet",id:43},{name:"phantom assassin",id:44},{name:"pugna",id:45},{name:"templar assassin",id:46},{name:"viper",id:47},{name:"luna",id:48},{name:"dragon knight",id:49},{name:"dazzle",id:50},{name:"clockwerk",id:51},{name:"leshrac",id:52},{name:"nature's prophet",id:53},{name:"'nature 39 s prophet'",id:53},{name:"lifestealer",id:54},{name:"dark seer",id:55},{name:"clinkz",id:56},{name:"omniknight",id:57},{name:"enchantress",id:58},{name:"huskar",id:59},{name:"night stalker",id:60},{name:"broodmother",id:61},{name:"bounty hunter",id:62},{name:"weaver",id:63},{name:"jakiro",id:64},{name:"batrider",id:65},{name:"chen",id:66},{name:"spectre",id:67},{name:"doom",id:69},{name:"ancient apparition",id:68},{name:"ursa",id:70},{name:"spirit breaker",id:71},{name:"gyrocopter",id:72},{name:"alchemist",id:73},{name:"invoker",id:74},{name:"silencer",id:75},{name:"outworld devourer",id:76},{name:"lycan",id:77},{name:"brewmaster",id:78},{name:"shadow demon",id:79},{name:"lone druid",id:80},{name:"chaos knight",id:81},{name:"meepo",id:82},{name:"treant",id:83},{name:"ogre magi",id:84},{name:"undying",id:85},{name:"rubick",id:86},{name:"disruptor",id:87},{name:"nyx assassin",id:88},{name:"naga siren",id:89},{name:"keeper of the light",id:90},{name:"io",id:91},{name:"visage",id:92},{name:"slark",id:93},{name:"medusa",id:94},{name:"troll warlord",id:95},{name:"centaur",id:96},{name:"magnus",id:97},{name:"timbersaw",id:98},{name:"bristleback",id:99},{name:"tusk",id:100},{name:"skywrath mage",id:101},{name:"abaddon",id:102},{name:"elder titan",id:103},{name:"legion commander",id:104},{name:"ember spirit",id:106},{name:"earth spirit",id:107},{name:"terrorblade",id:109},{name:"phoenix",id:110},{name:"oracle",id:111},{name:"techies",id:105},{name:"winter wyvern",id:112},{name:"arc warden",id:113},{name:"underlord",id:108}];

function setLeagueId(id) {
    leagueId = id;
}

// function loadLeagueMatches(league_id) {
//     return fantasyRepo.getMatchHistory(league_id).then(function(result) {
//         _matches = _(result.matches).map('match_id').map(_.toInteger).value();
//         //console.log('matches: ', _matches);
//         return _matches;
//     });
// }

function loadLeagueTeams(league_id) {
    console.log('loadLeagueTeams');
    leagueId = league_id;
    return fantasyRepo.getMatchHistory(leagueId).then(function(matches) {
        return fantasyRepo.getTeamInfo(leagueId).then(function(teams) {
            return fantasyRepo.getProPlayerList().then(function(players) {
                //console.log('getProPlayerList');
                _teams = _.map(teams, function(team) {
                    team.created = new Date(team.time_created * 1000);
                    team.members = _.map(team.members, function(m) {
                        var p = _.find(players, {account_id: m.account_id});
                        return _.extend(m, p, {active: true, 'stats': { 'averages': {kills: 0}, 'fantasy': {total: 0}}});
                    });
                    var teamMatches = _(matches).filter(function(m) {
                        return (m.radiant_team_id == team.team_id || m.dire_team_id == team.team_id);
                    }).orderBy('match_id', 'desc').map('match_id').value();
                    
                    if(teamMatches.length==0){
                        teamMatches = _(team.recent_match_ids).orderBy('desc').value();
                        console.log('teamMatches 2', team.name, teamMatches.length);
                    }
                    
                    team.tourneyMatches = _.take(_.map(teamMatches, _.toInteger), 5); //_(_matches).intersection(_.map(team.recent_match_ids, _.toInteger)).value();
                    return _.omit(team,['leagues_participated','top_match_ids','recent_match_ids']);
                });

                return _teams;
            });
            
        });
    });
}


function loadLeagueMatchDetails(match_ids) {
    var deferred = q.defer();
    var matches = [];
    var index = 0;
    //console.log('loadLeagueMatchDetails', match_ids);
    _.forEach(match_ids, function(match_id) {
        index++;
        setTimeout(function() {
            fantasyRepo.getMatchDetails(match_id).then(function(details) {
                loadMatchLog(match_id).then(function(match_stats) {
                    //console.log('opendota: ', match_stats[1]);
                    details.players = _.map(details.players, function(p) {
                        //console.log('match stats', p.account_id, p.kills);
                        var stats = _.find(match_stats, {hero_id: p.hero_id});
                        var statsExtended = _.extend(stats, p, {match_id: details.match_id });
                        return _.omit(statsExtended,['ability_upgrades']);
                    });
                    matches.push(details);
                    if(match_id == _.last(match_ids)){
                        return deferred.resolve(matches);
                    }
                    return details;
                });
            })
        }, apiThrottle * (index));
    });
    return deferred.promise;
}

function loadPlayerStats(team) {
    var deferred = q.defer();
    var index = 0;
    var received = 0;
    //console.log('loadPlayerStats', team.tourneyMatches.length);
    loadLeagueMatchDetails(team.tourneyMatches).then(function(matches) {
        var members = _.map(matches, function(m) {
            return m.radiant_team_id==team.team_id ? _.take(m.players, 5) : _.takeRight(m.players, 5);
        });
        var memberIds = _(_.flatten(members)).map('name').uniq().value();
        console.log(memberIds);
        
        _.forEach(team.members, function(member) {
            index++;
            //setTimeout(function() {
                //loadTournamentPlayerStats(member.account_id).then(function(stats) {
                    //console.log('stats for', member.account_id, stats.fantasy.total);
                    var averagePlayerStats = calculateFantasyPointsComplete(member, matches);
                    if(averagePlayerStats.remove){
                        //console.log('removing', member.account_id, team.members.length);
                        _.remove(team.members, member);
                        //console.log('team count', team.members.length);
                    }
                    else {
                        var playerStatsDecorated = decoratePlayerStats(averagePlayerStats);    
                        member.stats = _.extend({}, member.stats, playerStatsDecorated);
                        received++;
                    }
                                    
                    if(received>=team.members.length) {
                        //console.log('returning team stats');
                        return deferred.resolve(team);
                    }
                //});
            //}, index * apiThrottle);
        });
    });
    return deferred.promise;
}

function loadMatchLog(match_id) {
    return fantasyRepo.getOpenDotaMatch(match_id).then(parseMatchOpenDota);
    //return fantasyRepo.getDotaBuffMatchLog(match_id).then(parseMatchLog);//.then(function(match_log) {
        //console.log('match_log: ', match_log);
        //return match_log;
    //});
}

function loadProPlayerList() {
    return fantasyRepo.getProPlayerList().then(function(players) {
        return _.map(player, decoratePlayerStats);
    });
}

function loadTournamentPlayerStats(account_id) {
    return fantasyRepo.getTournamentPlayerStats(leagueId, account_id).then(function(stats) {
        return decoratePlayerStats(stats);
    });
}

function decoratePlayerStats(p) {
    try{
        var averages = {
            kills: _.round(p.kills_average || 0, 4),
            assist: _.round(p.assists_average || 0, 4),
            deaths: _.round(p.deaths_average || 0, 4),
            gpm: _.round(p.gpm_average || 0, 4),
            last_hits: _.round(p.last_hits_average || 0, 4),
            denies: _.round(p.denies_average || 0, 4),
            networth: _.round(p.networth_average || 0, 4),
            xpm: _.round(p.xpm_average || 0, 4),
            wards_placed: _.round(p.wards_placed_average || 0, 4),
            first_blood: _.round(p.first_blood_average || 0, 4),
            team_fight: _.round(p.team_fight_average || 0, 4),
            runes: _.round(p.runes_activated_average || 0, 4),
            towers: _.round(p.towers_killed_average || 0, 4),
            roshan: _.round(p.roshan_kills_average || 0, 4),
            camps_stacked: _.round(p.camps_stacked_average || 0, 4),
            stuns: _.round(p.stuns_average || 0, 4)
        };
        var fantasy = calculateFantasyPoints(averages);
        fantasy.total = _.round(_.reduce(fantasy, function(result, v, k) {
            return result += v;
        }), 4);
        //console.log('decoratePlayerStats END');
        return {
            'averages': averages,
            'fantasy': fantasy
        };
    }
    catch(err) {
        console.log('---------------------------');
        console.log('err decorate: ', err);
        console.log('---------------------------');
        return {};
    }
}

function calculateFantasyPoints(stats) {
    //console.log('decoratePlayerStats Start/End');
    try{
        return {
            kills: _.round(stats.kills * 0.3, 3),
            deaths: _.round(3 - (stats.deaths * 0.3), 3),
            last_hits: _.round((stats.last_hits + stats.denies) * 0.003, 3),
            gpm: _.round(stats.gpm * 0.002, 3),
            team_fight: _.round(3 * stats.team_fight, 3),
            wards: _.round(stats.wards_placed * 0.5, 4),
            runes: _.round(stats.runes * 0.25, 4),
            first_blood: stats.first_blood * 4,
            towers: stats.towers * 1,
            roshan: stats.roshan * 1,
            camps_stacked: stats.camps_stacked * 0.5,
            stuns: stats.stuns * 0.05

            
        };
    }
    catch(err){
        console.log('---------------------------');
        console.log('err fantasy: ', err);
        console.log('---------------------------');
        return {};
    }
    
}

function calculateFantasyPointsComplete(member, matches) {
    //console.log('calculateFantasyPointsComplete');
    //console.log('matches: ', matches.length);
    //console.log('member: ', member.account_id);
    try{
        var memberStats = _(matches).map('players').flatten().filter({account_id: member.account_id}).value();
        var team_kills_total = _.sum(_.map(matches, function(m) {
            var team = _(m.players).find({account_id: member.account_id}).player_slot < 100 ? 0 : 1;
            var team_kills = team == 0 ? m.radiant_score : m.dire_score;
            return team_kills;
        }));
        //console.log('kills_average',_.mean(_(memberStats).map('kills').value()));
        var averages = {
            account_id: member.account_id,
            kills_average: _.mean(_(memberStats).map('kills').value()),
            deaths_average: _.mean(_(memberStats).map('deaths').value()),
            assists_average: _.mean(_(memberStats).map('assists').value()),
            wards_placed_average: _.mean(_(memberStats).map('wards_placed').value()),
            first_blood_average: _.mean(_(memberStats).map('first_blood').value()),
            runes_activated_average: _.mean(_(memberStats).map('runes_activated').value()),
            last_hits_average: _.mean(_(memberStats).map('last_hits').value()),
            denies_average: _.mean(_(memberStats).map('denies').value()),
            gpm_average: _.mean(_(memberStats).map('gold_per_min').value()),
            //networth: _.mean(_(memberStats).map('networth').value()),
            xpm_average: _.mean(_(memberStats).map('xp_per_min').value()),
            team_fight_average: _.round((_.sum(_(memberStats).map('kills').value()) + _.sum(_(memberStats).map('assists').value())) / team_kills_total, 4),
            towers_killed_average: _.mean(_(memberStats).map('towers_killed').value()),

            roshan_kills_average: _.mean(_(memberStats).map('roshan_kills').value()),
            camps_stacked_average: _.mean(_(memberStats).map('camps_stacked').value()),
            stuns_average: _.mean(_(memberStats).map('stuns').value())

        };
        //console.log('averages', averages);
        return averages;
    }
    catch(err){
        //console.log('---------------------------');
        //console.log('ERROR fantasy complete: ', member.team_name, member.account_id);
        //console.log('---------------------------');
        return {remove: true};
    }
    
}

function parseMatchOpenDota(match) {
    var stats = _.map(match.players, function(p) {
        var player_stats = {
            name: p.name,
            account_id: p.account_id,
            player_slot: p.player_slot,
            hero_id: p.hero_id,
            camps_stacked: p.camps_stacked,
            roshan_kills: p.roshan_kills,
            towers_killed: p.tower_kills,

            kills: p.kills,
            deaths: p.deaths,
            assists: p.assists,
            gold_per_min: p.gold_per_min,
            xp_per_min: p.xp_per_min,

            last_hits: p.last_hits,
            denies: p.denies,
            wards_placed: p.obs_placed,
            runes_activated: p.rune_pickups,
            stuns: p.stuns,

            first_blood: _.some(match.objectives, {'type': 'CHAT_MESSAGE_FIRSTBLOOD', 'player_slot': p.player_slot}) ? 1 : 0

        };
        return player_stats;
    });
    
    return stats;
}


function parseMatchLog(log){
    //console.log('-- ParseMatchLog');
    var player_stats = [];
    var first_blood_hero = 0;
    _.forEach(log, function(e) {
        var event = _.words(_.trim(e.event));
        var log_event = false;
        var hero = getHeroId(_.words(e.hero), false);
        var hero_id = hero.id;
        var kills = 0;
        var ward_placed = 0;
        var rune_activated = 0
        var first_blood = 0;
        var tower_killed = 0;
        
        if(_.includes(e.event, 'killed')) { // _.event[1]=='killed' || event[2]=='killed'){   //kill event
            //console.log(e.time, 'kill', _.slice(event, 0, 6));
            var hero_index = _.indexOf(event, 'killed') + 2;
            hero = getHeroId(_.slice(event, hero_index, hero_index + 4), false);
            if(hero){
                log_event = true;
                kills = 1;
                var killed = getHeroId(_.words(e.hero), false).id;
                if(first_blood_hero==0){
                    first_blood = 1;
                    first_blood_hero = hero.id;
                }
            }
        }
        if(_.includes(e.event, 'activated the Top') || _.includes(e.event, 'activated the Bottom')) { // || _.join(_.takeRight(event, 4), ' ')=='placed a Sentry Ward'
            //console.log(e.time, 'rune', _.slice(event, 0, 6));
            log_event = true;
            rune_activated = 1;
        }
        if(_.includes(e.event, 'placed a  Observer Ward')) { // || _.join(_.takeRight(event, 4), ' ')=='placed a Sentry Ward'
            //console.log(e.time, 'ward', _.slice(event, 0, 6));
            log_event = true;
            ward_placed = 1;
        }
        if(_.includes(e.event, 'destroys') && _.includes(e.event, 'Tower')) { // || _.join(_.takeRight(event, 4), ' ')=='placed a Sentry Ward'
            //console.log(e.time, 'tower killed', _.slice(event, 0, 6));
            log_event = true;
            tower_killed = 1;
        }
        if(log_event && hero.id!=9999){
            
            var statsIndex = _.findIndex(player_stats, {hero_id: hero.id});
            if(statsIndex==-1){
                statsIndex = player_stats.push({hero_id: hero.id, hero_name: hero.name, kills: 0, first_blood: 0, wards_placed: 0, runes_activated: 0, towers_killed: 0}) - 1;
            }
            player_stats[statsIndex].kills += kills;
            player_stats[statsIndex].first_blood = player_stats[statsIndex].first_blood || first_blood;
            player_stats[statsIndex].wards_placed += ward_placed;
            player_stats[statsIndex].runes_activated += rune_activated;
            player_stats[statsIndex].towers_killed += tower_killed;
        }
    });
    //console.log('-- ParseMatchLog: FINSIHED');
    return player_stats;
}

function getHeroId(hero_name, isDebug) {
    var hero = '';
    if(hero_name.length > 0) {
        hero = _.find(_heroes, function(h) {
            //var ret_hero = _.first(_.words(h.name)) === _.toLower(hero_name);
            //if(!ret_hero) { console.log(h.name, hero_name); }
            
            var length = _.words(h.name).length;
            if(isDebug) {
                console.log('h.name: ', _.words(h.name), _.words(h.name).length);
                console.log('hero_n: ', _.take(_.map(hero_name,_.toLower), length));
            }
            
            var equal = _.isMatch(_.take(_.map(hero_name,_.toLower), length), _.words(h.name));
            if(equal && isDebug) {
                console.log('HERO MATCH FOUND!');
            }
            if(equal && isDebug && (h.name=='queen of pain' || h.name == 'keeper of the light')) {
                console.log('equal', equal, _.map(hero_name,_.toLower), _.words(h.name));
            }
            return equal;
        });
    }
    if(!hero && hero_name.length > 0) {
        //console.log('--- HERO NOT FOUND ---');
        hero = { id: 9999, name: hero_name };
        //console.log('--- ', _.map(hero_name,_.toLower));
    }
    //if(hero.id!=9999) { console.log('Found: ', _.toLower(_.first(_.words(hero_name))));}
    return hero;
}



module.exports =  {
    loadLeagueTeams: loadLeagueTeams,
    loadMatchLog: loadMatchLog,
    loadTournamentPlayerStats: loadTournamentPlayerStats,
    loadPlayerStats: loadPlayerStats
};
