'use strict';

var path = require('path');
var https = require('https');
var fantasyService = require('./fantasy.service')
var _ = require('lodash');
var q = require('q');
var firebase = require("firebase");
var jsonfile = require('jsonfile');
//firebase.database.enableLogging(true);

var saveFirebase = false;

var db = {};
setupFirebase();
var ref = db.ref('fantasy');

//var league = 4664;	//International 2016
//var league = 4874;	//Boston Major
var league = 5157;		//Kiev Major 2017

//fantasyService.loadMatchLog('2551542809').then(function(data) {
//	console.log('log: ', data);
//});

// fantasyService.loadLeagueMatches(league).then(function(matches) {
// 	console.log('matches:', matches);
// 	fantasyService.loadLeagueTeams(league).then(function(teams) {
// 		//console.log('teams',teams);
// 		//teams.tourneyMatches;
// 		loadStats(teams, 0);
// 	});
// });

fantasyService.loadLeagueTeams(league).then(function(teams) {
	teams = _.filter(teams, function(t) { return t.tourneyMatches.length>0 ;} );
	loadStats(teams, 0);
});

function loadStats(teams, index) {
	var team = teams[index];
	console.log('----------------------------------------------------------');
	console.log('LOAD:', team.name, index);
	if(!team) {
		console.log('no more teams!!!');
		return true;
	}
	loadTeamStats(team).then(function(saved) {
		index = index+1;
		loadStats(teams, index);
	});
}

function loadTeamStats(team) {
	var deferred = q.defer();
	fantasyService.loadPlayerStats(team).then(function(teamWithStats) {
		//console.log('Players With Stats:', _.map(teamWithStats.members, function(m) {return {'name': m.name, 'stats': m.stats.fantasy.total}})); //['name', 'stats.fantasy.total']
		teamWithStats.members = _.reject(teamWithStats.members, function(m) { return isNaN(m.stats.averages.kills); });
		teamWithStats.members =_.mapKeys(teamWithStats.members, function(o) { return o.account_id; });

		saveTeam(teamWithStats).then(function(team) {
			return deferred.resolve(true);
		});
	});
	return deferred.promise;
}

function setupFirebase(){
	firebase.initializeApp({
	  serviceAccount: "./Dota Fantasy-a071c75322d6.json",
	  databaseURL: "https://dota-fantasy.firebaseio.com"
	});
	
	db = firebase.database();
}

var d = new Date();
function saveTeam(team) {
	//return q.when(true);	//for testing only, wont' save anything
	var deferred = q.defer();
	if(saveFirebase)
	{
		
		try {
			
			deleteOldMemberRecords().then(function() {
				var playersRef = ref.child('members/' + d.getTime());
				playersRef.update(team.members).then(function(data) {
					console.log('SAVED!');
					return deferred.resolve(team);
				}).catch(function(error){
					console.log('somefink went wrong players...', error);
				});
			}).catch(function(error) {
				
			});

			console.log('SAVING...');
			
		}
		catch(err){
			console.log('ERROR Saving: ', err);
		}
	}
	else
	{
		saveJson(team).then(function() {
			return deferred.resolve(team);
		});;
	}
	return deferred.promise;
}

function cleanName(name) {
	return name.replace(/[.#$]/gi, ' ')
}

var alreadyDeleted = true;
function deleteOldMemberRecords() {
	if(alreadyDeleted) {
		return q.when(true);
	}
	var deferred = q.defer();
	var membersRef = ref.child('members');
	membersRef.remove().then(function() {
		alreadyDeleted = true;
		console.log('deleted old records.');
		return deferred.resolve(true);
	}).catch(function(error) {
		console.log('error deleting old records:', error);
		return deferred.reject(error);
	});
	return deferred.promise;
}


var membersToSave = [];
function saveJson(team) {
	var deferred = q.defer();
	
	var file = './data/members.json'
	var obj = {members: team.members};
	
	jsonfile.writeFile(file, obj, function (err) {
		console.error('err', err);
		return deferred.resolve(true);
	});
	return deferred.promise;
}


