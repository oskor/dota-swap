'use strict';

var _ = require('lodash');
var https = require('https');
var scrape = require("scrape");
var q = require('q');

var newResource = function() {

    var _steamApi = "https://api.steampowered.com";
    var _resourceUrl = "";
    var _apiKey = "";
    var _params = [];

    var _openDotaApi = "https://api.opendota.com/api";
    
    function createUrl() {
        //don't append any extra parameters if I'm only requesting a local json file.
        if(_resourceUrl.indexOf('.json')>0) {
            return _resourceUrl;
        }
        if(_resourceUrl.indexOf('matches')>0) {
            return _openDotaApi + _resourceUrl;// + '?' + addParams();
        }
        else {
            //return _resourceUrl + '?' + _apiKey + addParams();
            return _steamApi + _resourceUrl + '?' + _apiKey + addParams();
        }
    }
    function addParams() {
        return _params.map(function(param) {
            return '&' + param.name + '=' + param.key;
        });
    }
    function get(resourceUrl) {
        var deferred = q.defer();
        _resourceUrl = resourceUrl;
        var url = createUrl();
        console.log(url);
        https.get(url, function(res) {
            var body = '';
            res.on('data', (d) => {
                body += d;
            });
            res.on('end', (d) => {
                return deferred.resolve(JSON.parse(body));
            });
            
        }).on('error', (e) => {
            console.error('request error: ', e);
        });
        return deferred.promise;
    }

    var count = 0;
    return {
        get: get,
        withKey: function() {
            _apiKey = 'key=FBCD0235AE226119D146C8728F30AAC8';
            return this;
        },
        withParam: function(i, k) {
            _params.push({name: i, key: k});
            return this;
        }
    };
};

module.exports = {
    getTeamInfo: function(league_id) {
        return newResource().withKey().withParam('league_id',league_id).get('/IDOTA2Teams_570/GetTeamInfo/v1/').then(function(response) {
            return response.teams;
        });
    },
    getProPlayerList: function() {
        return newResource().withKey().get('/IDOTA2Fantasy_570/GetProPlayerList/v1/').then(function(response) {
            return response.player_infos;
        });
    },
    getTournamentPlayerStats: function(league_id, account_id) {
        return newResource().withKey().withParam('league_id',league_id).withParam('account_id',account_id).get('/IDOTA2Match_570/GetTournamentPlayerStats/v2/').then(function(response) {
            //console.log(response.result);
            return response.result || {};
        });
    },
    getMatchHistory: function(league_id) {
        return newResource().withKey().withParam('league_id',league_id).withParam('tournament_games_only',true).get('/IDOTA2Match_570/GetMatchHistory/v1/').then(function(response) {
            //console.log(response.result);
            return response.result.matches;
        });
    },
    getMatchDetails: function(match_id) {
        return newResource().withKey().withParam('match_id',match_id).get('/IDOTA2Match_570/GetMatchDetails/v1/').then(function(response) {
            //console.log(response.result);
            return response.result;
        });
    },
    getOpenDotaMatch: function(match_id) {
        return newResource().withParam('match_id',match_id).get('/matches/'+match_id).then(function(response) {
            //console.log(response.result);
            return response;
        });
    },
    getDotaBuffMatchLog: function(match_id) {
        var deferred = q.defer();
        var options = {url: 'www.dotabuff.com/matches/' + match_id + '/log'};
        //console.log(options.url);
        try {
            scrape.request(options, function (err, $) {
                //console.log('%s %s', $.cmd, $.args.join(' '));
                if (err){
                    console.error('err: ', err);
                    return deferred.resolve({});
                }
                //console.log('got dataz');
                //console.log('%s %s', meta.cmd, meta.args.join(' '));
                var log = [];
                
                $('div.event').each(function (div) {
                    //console.log(div);
                    //console.log('div: ', div);
                    var time = div.find('span.time').first() || {text: ''};
                    var hero = div.find('a').first() || {text: ''};
                    var event = div.find('div.event').first() || {striptags: ''};
                    log.push({time: time.text, event: event.striptags, hero: hero.text});
                });
                return deferred.resolve(log);
                
            });
            return deferred.promise;
        }
        catch(error) {
            console.log('error: ', error);
        }
        
        
    }
};
